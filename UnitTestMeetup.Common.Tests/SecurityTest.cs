﻿using System;
using NUnit.Framework;

namespace UnitTestMeetup.Common.Tests
{
    [TestFixture]
    public class SecurityTest
    {
        [Test]
        public void Login_SendEmptyEmail_EmailIsEmptyResult()
        {
            Security security = new Security();
            string email = String.Empty;
            string password = String.Empty;

            LoginResult result = security.Login(email, password);

            Assert.AreEqual(LoginResult.EmailIsEmpty, result);
        }

        [Test]
        public void Login_SendEmailWithoutDomain_EmailFormatIsInvalidResult()
        {
            Security security = new Security();
            string email = "person@";
            string password = String.Empty;

            LoginResult result = security.Login(email, password);

            Assert.AreEqual(LoginResult.EmailFormatIsInvalid, result);
        }
    }
}

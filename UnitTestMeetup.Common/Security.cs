﻿using System;
using System.Text.RegularExpressions;

namespace UnitTestMeetup.Common
{
    public class Security
    {
        private const string emailFormat = @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z";

        public LoginResult Login(string email, string password)
        {
            if (String.IsNullOrEmpty(email))
            {
                return LoginResult.EmailIsEmpty;
            }

            bool isEmailWellFormatted = Regex.IsMatch(email, emailFormat, RegexOptions.IgnoreCase);
            if (!isEmailWellFormatted)
            {
                return LoginResult.EmailFormatIsInvalid;
            }

            return LoginResult.LoggedIn;
        }
    }

    public enum LoginResult
    {
        EmailIsEmpty,
        EmailFormatIsInvalid,
        LoggedIn
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTestMeetup.Math
{
    public class Calculator
    {
        public int Calculate(string line)
        {
            if (line == string.Empty)
            {
                return 0;
            }

            return Convert.ToInt32(line.Split(',')[1]) + Convert.ToInt32(line.Split(',')[2]);
        }

        public int Add(string line)
        {
            char elementsDelimiter = ',';      

            string[] elements = line.Split(elementsDelimiter);

            int result = 0;
            for (int i = 0; i < elements.Length; i++)
            {
                string element = elements[i].Trim();

                if (!String.IsNullOrEmpty(element))
                {
                    if (!ElementIsInteger(element))
                    {
                        throw new Exception(string.Format("Element at {0} position in not an Integer. Element = {1}", i, element));
                    }

                    result += Convert.ToInt32(element);
                }               
            }

            return result;
        }

        private bool ElementIsInteger(string element)
        {
            int number;
            bool result = int.TryParse(element, out number);

            return result;
        }

        public double Sqrt(string line)
        {
            if (String.IsNullOrEmpty(line))
            {
                throw new Exception("Element not found");
            }

            double doubleNumber;
            bool elementIsDouble = Double.TryParse(line, out doubleNumber);
            if (!elementIsDouble)
            {
                throw new Exception("Element is invalid");
            }

            return System.Math.Sqrt(doubleNumber);
        }
    }
}

﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestMeetup.Math.Tests
{
    [TestClass]
    public class CalculatorTest
    {
        [TestMethod]
        public void Calculate_LineIsBlank_ResultIsZero()
        {
            Calculator calculator = new Calculator();
            string line = String.Empty;

            int result = calculator.Calculate(line);

            Assert.AreEqual(0, result);
        }

        [TestMethod]
        public void Calculate_SimpleSum_ResultIsCalculation()
        {
            Calculator calculator = new Calculator();
            string line = "+,1,2";

            int result = calculator.Calculate(line);

            Assert.AreEqual(3, result);
        }

        [TestMethod]
        public void Add_EmptyString_ResultIsZero()
        {
            Calculator calculator = new Calculator();
            string line = String.Empty;

            int result = calculator.Add(line);

            Assert.AreEqual(0, result);
        }

        [TestMethod]
        public void Add_OneSimpleElementNoComma_ResultIsElementSent()
        {
            Calculator calculator = new Calculator();
            string line = "1";

            int result = calculator.Add(line);

            Assert.AreEqual(1, result);
        }

        [TestMethod]
        public void Add_OneSimpleElementWithComma_ResultIsElementSent()
        {
            Calculator calculator = new Calculator();
            string line = "1,";

            int result = calculator.Add(line);

            Assert.AreEqual(1, result);
        }

        [TestMethod]
        public void Add_TwoElements_ResultIsCalculation()
        {
            Calculator calculator = new Calculator();
            string line = "1,2";

            int result = calculator.Add(line);

            Assert.AreEqual(3, result);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void Add_TwoElementsFirstIsCharacter_ResultIsInvalidElementException()
        {
            Calculator calculator = new Calculator();
            string line = "a,2";

            int result = calculator.Add(line);
        }

        [TestMethod]
        public void Add_FiveElements_ResultIsCalculation()
        {
            Calculator calculator = new Calculator();
            string line = "1,2,3,4,5";

            int result = calculator.Add(line);

            Assert.AreEqual(15, result);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void Add_FiveElementsCharacterInBetween_ResultIsInvalidElementException()
        {
            Calculator calculator = new Calculator();
            string line = "1,2,3,a,4,5";

            int result = calculator.Add(line);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void Sqrt_SendEmptyString_ThrowsElementNotFoundException()
        {
            Calculator calculator = new Calculator();
            string line = "";

            double result = calculator.Sqrt(line);
        }

        [TestMethod]
        public void Sqrt_SendZero_ResultIsZero()
        {
            Calculator calculator = new Calculator();
            string line = "0";
            double expected = 0;

            double result = calculator.Sqrt(line);

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void Sqrt_SendNine_ResultIsThree()
        {
            Calculator calculator = new Calculator();
            string line = "9";
            double expected = 3;

            double result = calculator.Sqrt(line);

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void Sqrt_SendCharacter_ThrowsInvalidElementException()
        {
            Calculator calculator = new Calculator();
            string line = "a";

            double result = calculator.Sqrt(line);
        }
    }
}

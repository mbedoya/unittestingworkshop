﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTestMeetup.HappyLoanDomain.Abstractions
{
    public interface IShoppingCartRepository
    {
        void SavePayment(int gatewayPaymentId, int UserId);
    }
}

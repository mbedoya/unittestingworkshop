﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTestMeetup.HappyLoanDomain.Abstractions
{
    public interface IPaymentGateway
    {
        int MakePayment(string cardNumber, string userNameInCard, DateTime cardExpirationDate, double paymentValue, string paymentDescription);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTestMeetup.HappyLoanDomain.Abstractions
{
    public interface IPersonRepository
    {
        bool Remove(int personID);
    }
}

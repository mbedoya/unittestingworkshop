﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTestMeetup.HappyLoanDomain.DTO
{
    public class CreditCard
    {
        public string Number { get; set; }
        public string OwnerName { get; set; }
        public string OwnerAddress { get; set; }
        public string VerificationCode { get; set; }
        public string ValidUntil { get; set; }
    }
}

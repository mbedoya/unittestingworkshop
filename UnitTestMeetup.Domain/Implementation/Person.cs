﻿using UnitTestMeetup.HappyLoanDomain.Abstractions;

namespace UnitTestMeetup.HappyLoanDomain.Implementation
{
    public class Person
    {
        private IPersonRepository repository;
        private ILogger logger;

        public Person(IPersonRepository repository, ILogger logger)
        {
            this.repository = repository;
            this.logger = logger;
        }

        public bool Remove(int personID)
        {
            bool personRemoved = repository.Remove(personID);
            logger.Info("Person removed");

            return personRemoved;
        }
    }
}

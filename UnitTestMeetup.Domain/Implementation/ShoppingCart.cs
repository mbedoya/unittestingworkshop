﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnitTestMeetup.HappyLoanDomain.Abstractions;
using UnitTestMeetup.HappyLoanDomain.DTO;

namespace UnitTestMeetup.HappyLoanDomain.Implementation
{
    public class ShoppingCart
    {
        IPaymentGateway paymentGateway;
        IShoppingCartRepository repository;
        IMailService mailService;

        public ShoppingCart(IPaymentGateway paymentGateway, IShoppingCartRepository repository, IMailService mailService)
        {
            this.paymentGateway = paymentGateway;
            this.repository = repository;
            this.mailService = mailService;
        }

        public int Checkout(User user, CreditCard creditCard)
        {
            if (String.IsNullOrEmpty(creditCard.Number))
            {
                throw new Exception("Credit card number must not be empty");
            }

            int paymentId = paymentGateway.MakePayment("", "", DateTime.Now, 100, "");

            if (paymentId <= 0)
            {
                throw new Exception("Payment Failed. Id Retrieved is not valid");
            }

            repository.SavePayment(paymentId, user.Id);
            mailService.Send(user.Email, "Payment Successful", "");

            return paymentId;
        }
    }
}

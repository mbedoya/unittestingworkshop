﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTestMeetup.Business
{
    public class PersonManager
    {
        public Tuple<bool, string> Delete(int personID)
        {
            if (personID == 3)
            {
                return new Tuple<bool, string>(true, String.Empty);
            }

            return new Tuple<bool, string>(false, "Person Not Found");
        }
    }
}

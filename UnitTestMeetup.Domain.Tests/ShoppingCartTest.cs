﻿using Moq;
using NUnit.Framework;
using System;
using UnitTestMeetup.HappyLoanDomain.Abstractions;
using UnitTestMeetup.HappyLoanDomain.DTO;
using UnitTestMeetup.HappyLoanDomain.Implementation;

namespace UnitTestMeetup.Domain.Tests
{
    [TestFixture]
    public class ShoppingCartTest
    {
        Mock<IPaymentGateway> doublePaymentGateway;
        Mock<IShoppingCartRepository> doubleRepository;
        Mock<IMailService> doubleMailService;
        User user;
        CreditCard creditCard;

        [SetUp]
        public void Init()
        {
            doublePaymentGateway = new Mock<IPaymentGateway>();
            doubleRepository = new Mock<IShoppingCartRepository>();
            doubleMailService = new Mock<IMailService>();
            user = new User() { Id = 1, Email = "peter@mail.com", Name = "Peter" };
            creditCard = new CreditCard() { Number = "4111333322221111", OwnerAddress = "Street Arizona", OwnerName = "Peter Smith", ValidUntil="05/18", VerificationCode="323" };
        }

        [Test]
        public void Checkout_PaymentDone_ReturnIsValidPaymentId()
        {
            doublePaymentGateway.SetReturnsDefault(1);
            ShoppingCart shoppingCart = new ShoppingCart(doublePaymentGateway.Object, doubleRepository.Object, doubleMailService.Object);

            int paymentID = shoppingCart.Checkout(user, creditCard);

            Assert.IsTrue(paymentID > 0);
        }

        [Test]
        public void Checkout_PaymentFailsIdIsLessThanZero_ThrowsException()
        {
            doublePaymentGateway.SetReturnsDefault(0);
            ShoppingCart shoppingCart = new ShoppingCart(doublePaymentGateway.Object, doubleRepository.Object, doubleMailService.Object);

            Assert.Throws<Exception>(() => shoppingCart.Checkout(user, creditCard));
        }

        [Test]
        public void Checkout_MailSentAfterSuccessfulPayment_SendMailMethodCalled()
        {
            doublePaymentGateway.SetReturnsDefault(1);
            ShoppingCart shoppingCart = new ShoppingCart(doublePaymentGateway.Object, doubleRepository.Object, doubleMailService.Object);

            int paymentID = shoppingCart.Checkout(user, creditCard);

            doubleMailService.Verify(m => m.Send(user.Email, "Payment Successful", ""));
        }

        [Test]
        public void Checkout_CreditCardNumberIsEmtpy_ThrowsException()
        {
            doublePaymentGateway.SetReturnsDefault(1);
            ShoppingCart shoppingCart = new ShoppingCart(doublePaymentGateway.Object, doubleRepository.Object, doubleMailService.Object);

            creditCard.Number = "";
            Assert.Throws<Exception>(() => shoppingCart.Checkout(user, creditCard));
        }

    }
}

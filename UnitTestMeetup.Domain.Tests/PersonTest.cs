﻿using NUnit.Framework;
using UnitTestMeetup.Domain.Tests.Doubles;
using UnitTestMeetup.HappyLoanDomain.Implementation;

namespace UnitTestMeetup.Domain.Tests
{
    [TestFixture]
    public class PersonTest
    {
        DoublePersonRepository doubleRepositorio;
        HappyLoanDoubleLogger doubleLogger;

        [SetUp]
        public void Init()
        {
            doubleRepositorio = new DoublePersonRepository();
            doubleLogger = new HappyLoanDoubleLogger();
        }

        [Test]        
        public void Remove_RemoveProcessSuccessfulyCompleted_ResultIsTrue()
        {
            Person person = new Person(doubleRepositorio, doubleLogger);
            int personIdToRemove = 3;

            bool result = person.Remove(personIdToRemove);

            Assert.IsTrue(result);
        }

        [Test]
        public void Remove_LoggerIsCalledAfterPersonIsRemoved_LoggerInfoMethodCalled()
        {
            Person person = new Person(doubleRepositorio, doubleLogger);
            int personIdToRemove = 3;

            bool result = person.Remove(personIdToRemove);

            Assert.IsTrue(doubleLogger.InfoCalled);
        }

        [Test]
        public void Remove_PersonIsRemovedFromDatabase_DatabaseCountDecreased()
        {
            Person person = new Person(doubleRepositorio, doubleLogger);
            int personIdToRemove = 3;
            int currentPeople = doubleRepositorio.PeopleCount;

            bool result = person.Remove(personIdToRemove);

            Assert.AreEqual(currentPeople-1, doubleRepositorio.PeopleCount);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnitTestMeetup.HappyLoanDomain.Abstractions;

namespace UnitTestMeetup.Domain.Tests.Doubles
{
    public class DoublePersonRepository : IPersonRepository
    {
        private int peopleCount;
        public int PeopleCount { get => peopleCount; }

        public DoublePersonRepository()
        {
            peopleCount = 3;
        }        

        public bool Remove(int personID)
        {
            peopleCount--;
            return true;
        }
    }
}

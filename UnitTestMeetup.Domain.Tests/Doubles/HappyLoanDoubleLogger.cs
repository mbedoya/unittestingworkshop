﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnitTestMeetup.HappyLoanDomain.Abstractions;

namespace UnitTestMeetup.Domain.Tests.Doubles
{
    public class HappyLoanDoubleLogger : ILogger
    {
        private bool infoCalled;
        public bool InfoCalled { get => infoCalled; }

        public void Error(string message)
        {
            
        }

        public void Info(string message)
        {
            infoCalled = true;
        }

        public void Warn(string message)
        {
            
        }
    }
}

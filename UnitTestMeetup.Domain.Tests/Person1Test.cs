﻿using Moq;
using NUnit.Framework;
using UnitTestMeetup.Domain.Tests.Doubles;
using UnitTestMeetup.HappyLoanDomain.Abstractions;
using UnitTestMeetup.HappyLoanDomain.Implementation;

namespace UnitTestMeetup.Domain.Tests
{
    [TestFixture]
    public class Person1Test
    {
        public void Remove_PersonIdIsZeroOrLower_ThrowsInvalidNumberException()
        {
            
        }

        [Test]
        public void Remove_LoggerIsCalledAfterPersonIsRemoved_LoggerInfoMethodCalled()
        {
            Mock<ILogger> doubleLogger = new Mock<ILogger>();
            Mock<IPersonRepository> doubleRepository = new Mock<IPersonRepository>();
            Person person = new Person(doubleRepository.Object, doubleLogger.Object);
            int personIdToRemove = 3;

            bool result = person.Remove(personIdToRemove);

            doubleLogger.Verify(m => m.Info("Person removed"));
        }

        [Test]
        public void Remove_ActionIsLogged_CustomClass_LoggerInfoIsCalled()
        {
            HappyLoanDoubleLogger doubleLogger = new HappyLoanDoubleLogger();
            Mock<IPersonRepository> doubleRepository = new Mock<IPersonRepository>();
            Person person = new Person(doubleRepository.Object, doubleLogger);
            int personID = 3;

            bool result = person.Remove(personID);

            Assert.IsTrue(doubleLogger.InfoCalled);
        }
    }
}

﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestMeetup.Business.Tests
{
    [TestClass]
    public class PersonManagerTest
    {
        [TestMethod]
        public void Delete_PersonIdExists_ResultIsTrue()
        {
            PersonManager personManager = new PersonManager();
            int personID = 3;

            Tuple<bool, string> result = personManager.Delete(personID);

            Assert.AreEqual(true, result.Item1);
        }

        [TestMethod]
        public void Delete_PersonIdDoesNotExist_ResultIsFalseWithMessage()
        {
            PersonManager personManager = new PersonManager();
            int personID = 1;

            Tuple<bool, string> result = personManager.Delete(personID);

            Assert.AreEqual(false, result.Item1);
        }
    }
}
